var webpack = require('webpack');
var path = require("path");

var vendors_d3 = [
    "d3-shape",
    "d3-color",
    "d3-scale",
    "d3-selection",
    "d3-hierarchy",
    "d3-interpolate",
    "d3-transition"
];

module.exports = {
    entry: {
        main: "./src/index.tsx",
        d3: vendors_d3
    },
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist"
    },
    devtool: "source-map",  // production : "eval",

    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {warnings: false},
            output: {comments: false},
            sourceMap: true
        }),
        new webpack.optimize.CommonsChunkPlugin({name: ["d3"], filename: "vendor.[name].js"})
    ],

    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".jsx"],
        modules: [
            "node_modules"
        ]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "ts-loader"
            }
        ]
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: [
        {
            'react': "React",
            'react-dom': "ReactDOM",
            'redux': "Redux",
            'react-redux': "ReactRedux",
            'react-addons-transition-group': "React.addons.TransitionGroup"
        }
    ]
};