import {tech} from "./tech";
import {Skill} from "../models/skill";

export const skill = [
	new Skill(
		"Programming",
		[
			` - 5+ years learning experience plus 2 years commercial experience of web front end development.`,
			` - An expert of ${tech.html5.html}, ${tech.css.CSS3}, ${tech.jsLib.javascript}(es5, es6) and JS frameworks, such as ${tech.jsLib.angular}, ${tech.jsLib.react}, ${tech.jsLib.jQuery}, ${tech.css.bootstrap}, ${tech.engine.nodeJS}.`,
			` - Familiar with many JS, CSS development tools and compilers such as ${tech.jsTool.grunt}, ${tech.jsTool.gulp}, ${tech.jsTool.babel}, ${tech.jsLib.typescript}, ${tech.jsTool.karma}, ${tech.jsTool.webpack}, and other languages such as ${tech.engine.PHP}, ${tech.app.java} and ${tech.app.objectC}.`,
			` - Commercial experience of continue integration and agile development.`
		]
	),
	new Skill(
		"Server",
		[
			`${tech.server.AWS}, ${tech.server.apache} and ${tech.server.IIS}`
		]
	),
	new Skill(
		"IDE",
		[
			`${tech.IDE.webstorm}, ${tech.IDE.eclipse}, ${tech.IDE.textMate}, ${tech.IDE.xCode}, ${tech.IDE.fireworks}, ${tech.IDE.dreamwaver} and ${tech.IDE.lightwave}`
		]
	),
	new Skill(
		"VersionControl",
		[
			"Git"
		]
	),
	new Skill(
		"Other",
		[
			`${tech.hobby.drawing}, ${tech.hobby._3dModelling} and ${tech.hobby._3dPrinting}`
		]
	)
];