
import {Education} from "../models/education";

export const education: Education[] = [
	new Education(new Date(2014, 2), new Date(2014, 11), "Performance Education", "Professional Year - IT"),
	new Education(new Date(2012, 2), new Date(2013, 11), "UOW", "Master of Computer Study"),
	new Education(new Date(2011, 2), new Date(2011, 11), "UOW", "Master of Information and Communication"),
	new Education(new Date(2008, 5), new Date(2010, 11), "University of Wollongong", "Bachelor of IT")
] as Education[];

