import {PersonInfo, Address} from "../models/personInfo";

export const personInfo = {
	"owner": new PersonInfo(
		"Lance (Yu) TIAN",
		"http://demo.lancety.net/download/me.jpg",
		"ITree (2014-2016)",
		"Front End Web App Developer",
		"lanceaaaaa@gmail.com",
		"0413507623",
		"https://au.linkedin.com/in/yu-tian-a6574742",
		"https://www.dropbox.com/s/0u6xqhaon38w230/resume.pdf?dl=0",
		new Address(
			"29 Norfolk Street",
			"Killara",
			"NSW",
			2071
		)
	),
	"referee": [
		new PersonInfo(
			"Daniel Radicheski",
			null,
			"ITree",
			"Software developer",
			null,
			"+61 2 4253 5444",
			"https://www.linkedin.com/in/daniel-radiceski-79492836",
			null,
			null
		),
		new PersonInfo(
			"Cristina Trkulja",
			null,
			"ITree",
			"Human Resources & Quality Manager",
			null,
			"+61 2 4253 5444",
			"https://www.linkedin.com/in/cristina-trkulja-8428b4",
			null,
			null
		)
	] as PersonInfo[]
};


