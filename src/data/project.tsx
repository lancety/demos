import {Project, ProjectAction, ProjectActionType} from "../models/project";
import {tech} from "./tech";
export const project = [
    new Project(
        "major",
        "3D game terrain generator",
        "Babylonjs, private project",
        "img/model-terrain.jpg",
        [
            tech.jsLib.babylonjs, tech.html5.canvas, "Plasma Fractal"
        ],
        "A 3d terrain generator that use seed to generate consistent terrain. Which finally will be part of a fully functional web 3d MMO game engine",
        [
            "Auto generated terrain that is consistent base on a specific seed.",
            "Terrain is generated base on a heightmap which is generated on canvas",
            "Terrain size represent 10KM * 10Km real world area, in type of forest, desert, beach， highland and glacier",
            "Only Minimised code, source code not available at this stage."
        ],
        null,
        new Date(2017,0),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/model-terrain/"),
        null
    ),
    new Project(
        "major",
        "3D model viewer",
        "Babylonjs",
        "img/logo_babylonjs.png",
        [
            tech.jsLib.babylonjs, tech.html5.cache, tech.jsTool.webpack
        ],
        "A 3D model viewer which used babylonjs, model was made by myself.",
        [
            "Used scene loader",
            "Used camera, light, skybox and manifest cache"
        ],
        null,
        new Date(2017, 0),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/model-viewer"),
        "https://gitlab.com/lancety/model-viewer"
    ),
    new Project(
        "prototype",
        "Mobile wizard",
        "react mobile web app",
        "img/mobile_wizard.png",
        [
            tech.jsLib.react, tech.jsLib.redux,
            tech.html5.responsive, tech.css.CSS3
        ],
        "Card list style mobile wizard, wizard has a circle navigation bar represent each topic, also each topic is displayed as card panel, where shows title and completion state of each topic.",
        [
            "Circle item animation when select circle of navigation bar",
            "Topic card UI animation and scale when select a different topic",
            "Responsive layout that works on different size device",
            "Implemented base on React Redux lib"
        ],
        null,
        new Date(2016, 11),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/mobile_wizard"),
        "https://gitlab.com/lancety/mobile-wizard"
    ),
    new Project(
        "major",
        "Lancety Demo site",
        "ver.3",
        "img/demoSite_v3.png",
        [
            tech.jsLib.react, tech.jsLib.redux, tech.jsLib.typescript,
            tech.css.bootstrap, tech.css.LESS, tech.html5.responsive,
            tech.jsLib.d3js
        ],
        "Create a web page for demoing my projects",
        [
            "Responsive design",
            "Filtering projects base on category, using redux state change",
            "Print style design"
        ],
        null,
        new Date(2016, 9),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/demo/"),
        "https://gitlab.com/lancety/demos"
    ),
    new Project(
        "prototype",
        "Progress Bar",
        "Angular 2 practise",
        "img/progressBar.png",
        [
            tech.jsLib.angular, tech.jsLib.typescript, tech.jsTool.karma
        ],
        "Implementing angular 2 progress bar",
        [
            "Auto generating bars, random button value and max value",
            "Progress bar @input and @output are working",
            "percentage base on value / max value"
        ],
        null,
        new Date(2016, 9),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/progressBar/"),
        "https://gitlab.com/lancety/progressBarEditor"
    ),
    new Project(
        "prototype",
        "Take Away Manager",
        null,
        "img/takeAwayManager.png",
        [
            tech.jsLib.angular, tech.jsLib.typescript, tech.jsTool.grunt,
            tech.engine.nodeJS, tech.engine.mongoose, tech.engine.mongodb, tech.html5.responsive
        ],
        "Managing clients, products(food menu), order and delivery (through google map API)",
        [
            "Client and Product instance update, add and remove",
            "Angular 2 router config",
            "Language change without refresh"
        ],
        null,
        new Date(2016, 4),
        new ProjectAction(ProjectActionType.TRY, "http://node.lancety.net"),
        "https://gitlab.com/lancety/takeAwayManager"
    ),
    new Project(
        "major",
        "Self Service Portal",
        "ITree North Wollongong",
        "img/default.png",
        [
            tech.jsLib.angular, tech.jsLib.typescript, tech.jsLib.jQuery, tech.jsLib.jQueryUI,
            tech.jsTool.grunt, tech.jsTool.jsLint, tech.jsTool.karma, tech.jsTool.protractor,
            tech.css.bootstrap, tech.engine.nodeJS, tech.engine.msSQL, tech.app.java, tech.html5.responsive
        ],
        "Making angular modules, such as package, form, inputs, wizard, dashboard, which can be used by product integration team to make single page application that working together with back end system through c# server.",
        [
            "Responsive package design base on flex-box feature of css3",
            "Package, wizard and dashboard component which operate base on server side business rules and validation"
        ],
        "Contribute to all step of development: requirement, design, implementation, testing and release process",
        new Date(2014, 7),
        new ProjectAction(ProjectActionType.IMAGE)
    ),
    new Project(
        "prototype",
        "Lancety Demo site",
        "ver.2",
        "img/demoSite_v2.png",
        [
            tech.jsLib.angular, tech.css.bootstrap, tech.html5.responsive
        ],
        "Creating a web page for demoing my projects",
        [
            "Bootstrap grid style project list",
            "Added Facebook and LinkedIn share link"
        ],
        null,
        new Date(2014, 6),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/showBoard/")
    ),
    new Project(
        "prototype",
        "Media Query",
        "Practise and learn",
        "img/mediaQuery.png",
        null,
        "Practising media query usage",
        [
            "Better understanding of media query standard",
            "Gathering key info of device screen"
        ],
        null,
        new Date(2014, 6),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/_fun/mediaQuery.html")
    ),
    new Project(
        "prototype",
        "Map Path Generator",
        "Generate map path data",
        "img/mapPathGenerator.png",
        [
            tech.jsLib.jQuery, tech.css.bootstrap, tech.jsApi.googleMaps
        ],
        "To generate map node/path data which can be used in shortest path finding algorithm of other application",
        [
            "Set different path type (bus, drive, walk) and direction (single, two-way)",
            "Create map node base on google map canvas",
            "Import, export map data",
            "Set default map location of the generator/editor"
        ],
        null,
        new Date(2014, 5),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/tMapPathEditor/")
    ),
    new Project(
        "major",
        "Exchange order system",
        "ver.2",
        "img/exchange_v2.png",
        [
            tech.jsLib.jQuery, tech.jsLib.angular, tech.css.bootstrap, tech.engine.PHP, tech.engine.mySQL
        ],
        "Helping the system owner to calculate exchange order detail when placing orders, and managing orders with a login system",
        [
            "Login to PHP server to view order history",
            "Angular components",
            "Calculate order detail base on exchange rate which is received from ajax call"
        ],
        null,
        new Date(2014, 5),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/exchange/")
    ),
    new Project(
        "prototype",
        "IOS Flickr map viewer",
        "Uni course assignment",
        "img/flickerMapViewer.png",
        [
            tech.app.objectC
        ],
        "Assignment requires to show photos base on location, where photos come from the result of flickr API call",
        [
            "Set location and key words, store in application",
            "Sending request by using API, receive json and display as image list or show image pre-view on map."
        ],
        null,
        new Date(2013, 10),
        new ProjectAction(ProjectActionType.VIDEO, "https://www.youtube.com/watch?v=vDNkhBru7aM")
    ),
    new Project(
        "prototype",
        "OSX Exchange Calculator",
        "Base on real time rate",
        "img/osxExchangeCalculator.png",
        [
            tech.engine.PHP, tech.engine.mySQL, tech.app.objectC
        ],
        "An OSX application that frequently send request to php server to get most recent exchange rate info and calculate.",
        [
            "Application send request and receive JSON exchange",
            "PHP server keep recording most recent exchange rate from internet resource",
            "Keep storing exchange rate to mysql"
        ],
        null,
        new Date(2013, 10),
        new ProjectAction(ProjectActionType.IMAGE, "img/osxExchangeCalculator.png")
    ),
    new Project(
        "prototype",
        "IOS UOW Navigator",
        "Uni course group assignment",
        "img/iosUowNavigator.png",
        [
            tech.app.objectC, tech.engine.PHP, tech.engine.mySQL
        ],
        "University of wollongong navigator which Used shortest path finding algorithm that was learned in another uni course",
        [
            "All building data are added",
            "Walking and driving path data are added",
            "Path navigation works"
        ],
        "Design database, implement UI and interaction, implement GMSMaps feature",
        new Date(2013, 9),
        new ProjectAction(ProjectActionType.VIDEO, "https://www.youtube.com/watch?v=sBQlT0Id3cs")
    ),
    new Project(
        "prototype",
        "Web Dictionary",
        "Currently not fully working",
        "img/webDic_1.png",
        [
            tech.jsLib.jQuery, tech.html5.localStorage, tech.css.CSS3, tech.engine.PHP, tech.engine.mySQL
        ],
        "Web dictionary can store searched word, group by date, then allow user to start test base on searched words, user can type word as answer or choose one from 4 possible answers.",
        [
            "Search words on google dictionary and forvo, receive words' detail, and mp3 audio",
            "Store searched words, and group by date",
            "Test page which list searched word's with random order"
        ],
        null,
        new Date(2013, 4),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/dic/wwwroot/")
    ),
    new Project(
        "prototype",
        "Terrain Data Generator",
        "For web game",
        "img/terrainDataGenerator.png",
        [
            tech.jsLib.jQuery, tech.html5.canvas
        ],
        "Find a way to generate consistent game map data base on fix group of numbers",
        [
            "Generated map includes hill/mountain (Red) and forest/trees (Green)",
            "fix group of number always generate same map data"
        ],
        null,
        new Date(2013, 1),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/cavemen/client/builder/math.html")
    ),
    new Project(
        "hobby",
        "Gundam 3D Modelling and Animation",
        "Uni course assignment",
        "img/gundam_3d.png",
        [
            tech.IDE.lightwave, tech.IDE.fireworks
        ],
        "Choose a topic (Gundam), make 3d model, and add animation which shows skeleton connection works",
        [
            "Gundam model and animation is completed"
        ],
        null,
        new Date(2012, 10),
        new ProjectAction(ProjectActionType.VIDEO, "https://www.youtube.com/watch?v=r6jVz3HaRX4")
    ),
    new Project(
        "prototype",
        "webGameFPS",
        null,
        "img/webGameFps.png",
        [
            tech.html5.canvas
        ],
        "Checking possibility of making a 2d web game with 60 FPS",
        [
            "Frame based approach implemented",
            "Pixel game grid zoom implemented"
        ],
        null,
        new Date(2012, 9),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/cavemen/client/ui.html")
    ),
    new Project(
        "hobby",
        "Wacom Cintiq Drawing",
        "Tiger Kids drinking bottle",
        "img/wacomCintiqDrawing_bottle.png",
        [
            tech.hobby.drawing, tech.IDE.sketchBookPro
        ],
        "Drawing drinking bottle by using a wacom cintiq device",
        null,
        null,
        new Date(2012, 3),
        new ProjectAction(ProjectActionType.IMAGE, "img/wacomCintiqDrawing_bottle.png")
    ),
    new Project(
        "prototype",
        "Exchange order system",
        "ver.1",
        "img/exchange_v1.png",
        [
            tech.jsLib.javascript, tech.jsLib.jQuery, tech.jsLib.rapheal, tech.css.CSS3, tech.engine.PHP, tech.engine.mySQL
        ],
        "Helping the system owner to calculate exchange order detail when placing orders, and managing orders with a login system",
        [
            "Login to PHP server to view order history",
            "Calculate order detail base on exchange rate which is received from ajax call"
        ],
        null,
        new Date(2012, 1),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/exchange/")
    ),
    new Project(
        "prototype",
        "Lancety Demo site",
        "ver.1",
        "img/demoSite_v1.png",
        [
            tech.jsLib.javascript, tech.jsLib.jQuery, tech.jsLib.rapheal
        ],
        "Creating a web page for demoing my projects",
        [
            "Icon style project list",
            "Used SVG js lib (Rapheal) for animating interaction to the icons",
            "Added facebook link"
        ],
        null,
        new Date(2012, 0),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/mypro/wwwroot/")
    ),
    new Project(
        "prototype",
        "Web UOW Navigator",
        null,
        "img/webUowNavigator.png",
        [
            tech.jsLib.javascript, tech.jsLib.jQTouch, tech.html5.canvas
        ],
        "Make a web navigation app base on the map of wollongong university",
        [
            "Made a map tool for generating map path data",
            "Used a-star path finding algorithm",
            "Set 2 point, it will show you the path!"
        ],
        null,
        new Date(2012, 0),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/_fun/mapfun/")
    ),
    new Project(
        "major",
        "Online Drawing Chat",
        "Multi user sync drawing (server down)",
        "img/onlineDrawingChat.png",
        [
            tech.jsLib.jQuery, tech.html5.webSocket, tech.html5.canvas, tech.engine.PHP
        ],
        "Create a online multi user drawing application, that can chat by text, or view others' real time drawing",
        [
            "real time drawing use web socket and php socket",
            "select color, eraser, clean whole board, set user name"
        ],
        null,
        new Date(2011, 8),
        new ProjectAction(ProjectActionType.TRY, "http://demo.lancety.net/_fun/drawchart/client.html")
    ),
    new Project(
        "major",
        "M&B game statistic system",
        "for http://bbs.mountblade.com.cn/",
        "img/m&b_v1.png",
        [
            tech.jsLib.javascript, tech.jsLib.jQuery, tech.jsLib.rapheal, tech.engine.PHP, tech.engine.mySQL
        ],
        "Making a web system calculating and showing player confront summary and history for \"Mount and Blade\". (Project source code was not saved, but version release notes still can be found in their cn web site)",
        [
            "PHP server collecting game PVP log(txt), store in mysql",
            "Method for summarizing PVP data filter by weapon type, killer and date",
            "Ranking system sort by date, weapon and server",
            "Player info board which only show single player's summary",
            "real time server info, updated by ajax calls"
        ],
        null,
        new Date(2010, 6),
        new ProjectAction(ProjectActionType.IMAGE, "img/m&b_v1.png")
    ),
    new Project(
        "hobby",
        "CharcoalDrawing",
        "Portrait",
        "img/charcoalDrawing.png",
        [
            tech.hobby.drawing
        ],
        "Several portrait charcoal drawing",
        null,
        null,
        new Date(2010, 0),
        new ProjectAction(ProjectActionType.IMAGE, "img/charcoalDrawing_d.jpg")
    )
];