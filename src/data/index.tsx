import {education} from "./education";
import {timeline} from "./timeline";
import {personInfo} from "./personInfo";
import {project} from "./project";
import {skill} from "./skill";
import {tech, tech2d} from "./tech";
import {AppSate} from "../models/index";

const initState: AppSate = {
	tech,
	tech2d,
	personInfo,
	education,
	skill,
	timeline,
	project: {
		project,
		projectPriority: "major"
	}
};

export default initState;