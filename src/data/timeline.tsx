import {Activity} from "../models/timeline";
import {tech} from "./tech";

export const timeline: Activity[] = [
	new Activity(
		new Date(2017, 0),
		`Work on implementing ${tech.jsLib.babylonjs}, GLSL based terrain engine, support 8 different and configurable terrain types, fully random generated ground shape, minimised resources required from server. Which is a important feature of 3D ${tech.html5.webGL} MMO games.`
	),
	new Activity(
		new Date(2017, 0),
		`Made a ${tech.jsLib.babylonjs} 3D model viewer. Check this url: http://demo.lancety.net/model-viewer/`
	),
	new Activity(
		new Date(2016, 11),
		`Started learning ${tech.html5.webGL} related knowledge, finished the official and online tutorial of ${tech.jsLib.threejs} and ${tech.jsLib.babylonjs} library.`
	),
	new Activity(
		new Date(2016, 11),
		`Worked on a job application skill test case which requires building page for mobile device. It should have a topic card list view, animation when select different topic, also completed state should be shown when click button. Used ${tech.jsLib.react}, ${tech.jsLib.redux}, ${tech.html5.responsive}, ${tech.css.CSS3} transform and transition.`
	),
	new Activity(
		new Date(2016, 10),
		`Improved my demo site by adding ${tech.jsLib.d3js} sunburst chart for presenting my skills in group of html, javascript, os, server, hobby, etc. Supporting interesting animation`
	),
	new Activity(
		new Date(2016, 9),
		`Upgraded my demo site to ver.3 using ${tech.jsLib.react}, ${tech.jsLib.redux} and ${tech.jsLib.typescript}`
	),
	new Activity(
		new Date(2016, 7),
		`Migrated my demo site from VPS server of iiNet to ${tech.server.AWS}, used EC2, RDS, DMS, S3`
	),
	new Activity(
		new Date(2016, 4),
		`Started 'TakeAwayManager' prototype development, for practicing ${tech.jsLib.angular} 2, ${tech.engine.nodeJS} and ${tech.engine.mongoose}.`
	),
	new Activity(
		new Date(2014, 7),
		`Started working on ITree Self Service Portal project from scratch. Used ${tech.jsLib.angular}, ${tech.jsLib.jQueryUI} plugIns, ${tech.jsLib.typescript} since angular 1.5, ${tech.jsTool.grunt} for auto-testing and deployment, ${tech.jsTool.jsLint} for checking code quality, ${tech.jsTool.karma}, ${tech.jsTool.protractor} for testing, ${tech.css.bootstrap} for styling, in IDE ${tech.IDE.webstorm}`
	),
	new Activity(
		new Date(2014, 7),
		`Marking path node on ${tech.html5.canvas} which is on top of a ${tech.jsApi.googleMaps} canvas layer, outcome  is a path node object that can be used for short path finding algorithm. Date stored in ${tech.html5.localStorage}`
	),
	new Activity(
		new Date(2014, 5),
		`Started developing SPA by using ${tech.jsLib.angular}, ${tech.css.bootstrap}, ${tech.engine.PHP} and ${tech.engine.mySQL}, upgrade online exchange shop (ver.3)`
	),
	new Activity(
		new Date(2013, 11),
		`Finished IOS and Android development course, made UOW Navigator by using ${tech.app.objectC} and ${tech.IDE.xCode}`
	),
	new Activity(
		new Date(2013, 6),
		`Finished ${tech.engine.PHPSmart}, ${tech.engine.PHPZend}, ${tech.app.javaServlet} university courses`
	),
	new Activity(
		new Date(2013, 5),
		`Made a online dictionary by using ${tech.jsLib.jQuery} google dictionary, which allow user to test if then can remember the words which have been searched before`
	),
	new Activity(
		new Date(2012, 11),
		`Finished ${tech.app.java}, ${tech.hobby._3dModelling} university courses`
	),
	new Activity(
		new Date(2012, 8),
		`Made a 60 frame  prototype to learn how to design a pixel html5 web game`
	),
	new Activity(
		new Date(2012, 7),
		`Migrate demo site from my own windows server to iiNet VPS server, started maintaining this ${tech.os.ubuntu} server through ssh`
	),
	new Activity(
		new Date(2012, 2),
		`Upgrade online exchange shop (ver.2). Used ${tech.jsLib.jQuery}, ${tech.jsLib.rapheal}, ${tech.css.CSS3} and ${tech.engine.PHP} to provide exchange rate data from ${tech.engine.mySQL}.`
	),
	new Activity(
		new Date(2012, 1),
		`Made my demo site (ver.1), used ${tech.jsLib.jQuery}, ${tech.jsLib.rapheal}, ${tech.css.CSS3}`
	),
	new Activity(
		new Date(2011, 11),
		`Graduated at UOW - Major Master of Information and Communication Technology`
	),
	new Activity(
		new Date(2011, 5),
		`Made a mobile e-learning system which have a editable timetable, teaching material auto-delivery feature, and campus navigation system. Used ${tech.jsLib.jQTouch}, ${tech.jsTool.phoneGap}, ${tech.app.objectC} and ${tech.IDE.xCode}`
	),
	new Activity(
		new Date(2011, 4),
		`Made a multi user online drawing chat system, used ${tech.html5.canvas}, ${tech.html5.webSocket}, ${tech.css.CSS3} and ${tech.engine.PHP} socket`
	),
	new Activity(
		new Date(2010, 11),
		`Graduated at UOW - Major Bachelor of Information Technology`
	),
	new Activity(
		new Date(2010, 6),
		`Designed and developed a game server statistic system for http://bbs.mountblade.com.cn/, used ${tech.jsLib.jQuery}, ${tech.css.CSS3}, ${tech.engine.mySQL} and ${tech.engine.PHP}`
	),
	new Activity(
		new Date(2009, 2),
		`Hosting my own window server, started maintaining long term running ${tech.server.IIS} server`
	),
	new Activity(
		new Date(2008, 8),
		`Started learning ${tech.engine.msSQL} in uni projects`
	),
	new Activity(
		new Date(2007, 10),
		`Graduated at Sydney UTS InSearch - Major Information Technology`
	),
	new Activity(
		new Date(2006, 10),
		`Started using ${tech.IDE.dreamwaver} and ${tech.IDE.fireworks} for creating web pages`
	),
	new Activity(
		new Date(2006, 10),
		`Started learning SQL and database, used ${tech.engine.msAccess}, ${tech.engine.ASP} creating web system in ${tech.server.IIS} server`
	),
	new Activity(
		new Date(2006, 3),
		`Registered lancety.net and lancety.com domain`
	),
	new Activity(
		new Date(2005, 7),
		`Started learning ${tech.html5.html}`
	)
] as Activity[];

