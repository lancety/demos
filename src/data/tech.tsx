export const tech = {
	"html5": {
		html: "html",
		ES6: "ES6",
		canvas: "canvas",
		SVG: "SVG",
		webGL: "WebGL",
		webSocket: "webSocket",
		localStorage: "localStorage",
		cache: "cache",
		responsive: "responsive",
		ajax: "ajax"
	},
	"css": {
		CSS3: "CSS3",
		bootstrap: "bootstrap",
		LESS: "LESS",
		SASS: "SASS"
	},
	"jsLib": {
		javascript: "javascript",
		typescript: "typescript",
		angular: "angular",
		react: "react",
		redux: "redux",
		jQuery: "jQuery",
		jQTouch: "jQTouch",
		jQueryUI: "jQueryUI",
		rapheal: "rapheal",
		d3js:"d3JS",
		threejs: "ThreeJS",
		babylonjs: "BabylonJS"
	},
	"jsTool": {
		grunt: "grunt",
		gulp: "gulp",
		babel: "babel",
		webpack: "webpack",
		karma: "karma",
		protractor: "protractor",
		jsLint: "jsLint",
		phoneGap: "phoneGap",
	},
	"jsApi": {
		googleMaps: "googleMaps",
		facebook: "facebook"
	},
	"server": {
		AWS: "AWS",
		apache: "apache",
		IIS: "IIS",
		VPS: "VPS"
	},
	"engine": {
		nodeJS: "nodeJS",
		PHP: "PHP",
		PHPSmart: "PHPSmart",
		PHPZend: "PHPZend",
		mySQL: "mySQL",
		msSQL: "msSQL",
		msAccess: "msAccess",
		ASP: "ASP",
		mongodb: "mongodb",
		mongoose: "mongoose"
	},
	"app": {
		objectC: "objectC",
		swift: "swift",
		java: "java",
		javaServlet: "javaServlet"
	},
	"os": {
		osx: "osx",
		windows: "windows",
		ubuntu: "ubuntu"
	},
	"IDE": {
		xCode: "xCode",
		textMate: "textMate",
		webstorm: "webstorm",
		eclipse: "eclipse",
		fireworks: "fireworks",
		dreamwaver: "dreamwaver",
		lightwave: "lightwave",
		sketchBookPro: "sketchBookPro"
	},
	"hobby": {
		drawing: "drawing",
		_3dModelling: "_3dModelling",
		_3dPrinting: "_3dPrinting"
	}
};

export const techSorted = {
	"html": {
		"html5": tech.html5,
		"css": tech.css
	},
	"js": {
		"jsLib": tech.jsLib,
		"jsTool": tech.jsTool,
		"jsApi": tech.jsApi
	},
	"server": {
		"server": tech.server,
		"engine": tech.engine
	},
	"app": tech.app,
	"os": tech.os,
	"IDE": tech.IDE,
	"hobby": tech.hobby
};

export interface TechObject {
	name: string;
	parent: string;
	root: string;
}

const convertTreeTo2D = (item: any, root?: string, parent?: string): {[key: string]: TechObject} => {
	if (!root && !parent) {
		let tmp2D = {};
		for (let subItemKey in item) {
			if (item.hasOwnProperty(subItemKey)) {
				(Object as any).assign(tmp2D, convertTreeTo2D(item[subItemKey], subItemKey, subItemKey));
			}
		}
		return tmp2D;
	} else {
		if (typeof item === "object") {
			let traverse = {};
			for (let subItemKey in item) {
				if (item.hasOwnProperty(subItemKey)) {
					(Object as any).assign(traverse, convertTreeTo2D(
						item[subItemKey],
						root,
						typeof item[subItemKey] === "object" ? subItemKey : parent
					));
				}
			}
			return traverse;
		} else {
			return {
				[item]: {
					name: item,
					parent: parent,
					root: root
				}
			}
		}
	}
};

export const tech2d = convertTreeTo2D(techSorted);


export interface ChatDataTree {
	name: string,
	size?: number,
	children?: ChatDataTree[]
}

const convertSortedToTree = (objProp: string, object: any): any => {
	if (typeof object === "object") {
		let obj2tree: ChatDataTree = {
			name: objProp,
			children: []
		};

		for (let key in object) {
			if (object.hasOwnProperty(key)) {
				obj2tree.children.push(convertSortedToTree(key, object[key]));
			}
		}

		return obj2tree;
	} else {
		return {
			name: object,
			size: 1
		}
	}
};

export const tech4chat = convertSortedToTree("Technology", techSorted);