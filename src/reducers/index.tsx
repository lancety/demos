import {combineReducers} from "redux";
import project from "./project";
import initState from "../data/index";
import {AppSate} from "../models/index";

let reducerCallbacks: any = {};

// static reducers
for (let key in initState) {
	reducerCallbacks[key] = () => initState[key];
}

// dynamic reducers
const INFO_LEVEL_CHANGE = "INFO_LEVEL_CHANGE";
export const infoLevelChange = (level: string) => {
	return {
		type: INFO_LEVEL_CHANGE,
		level: level
	}
};
reducerCallbacks.infoLevel = (state: string = "short", action: any) => {
	switch (action.type) {
		case INFO_LEVEL_CHANGE:
			return action.level;
		default:
			return state;
	}
};
reducerCallbacks.project = project;

const reducer = combineReducers(reducerCallbacks);

export default reducer;