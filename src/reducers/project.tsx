import {project} from "../data/project";
import {Action} from "redux/index";
import {ProjectState} from "../models/project";
import {PROJECT_FILTER_UPDATE} from "../actions/project";

const initProjectState: ProjectState = {
	project: project,
	projectPriority: "major"
};

export default function projectReducer(state: ProjectState = initProjectState, action: Action) {
	switch (action.type) {
		case PROJECT_FILTER_UPDATE:
			return {
				project: state.project,
				projectPriority: action["filter"]
			} as ProjectState;
		default:
			return state;
	}
}