import * as React from "react";

export interface iElementFilterConfig {
	substringList: {[key: string]: any},
	string: string,
	tagName: string,
	getClassFn: (object: {[key: string]: any}) => string
}

export function util_insertElement(config: iElementFilterConfig): any {
	let words: string[] = config.string.split(" ");
	let tagName = config.tagName || "span";

	// config.string will be split into array,
	// and being converted to given dom name instance if matched key words
	let wordJsx: any[] = [];

	for (let word of words) {
		let matchResult: RegExpMatchArray = word.match(/[_a-zA-Z\d]+/);
		let pureWord = matchResult === null ? null : matchResult[0];

		if (pureWord && config.substringList[pureWord]) {
			wordJsx.push(
				React.createElement(
					tagName,
					{key: wordJsx.length},
					<span key="" className={config.getClassFn(config.substringList[pureWord])}>{pureWord}</span>
				)
			);

			if (pureWord.length !== word.length) {
				wordJsx.push(word.replace(pureWord, ""));
			}
		} else {
			wordJsx.push(word);
		}

		wordJsx.push(" ");
	}

	return wordJsx;
}

export function arraySentenceConnector(index: number, length: number): string {
	return index === length - 2 ? " and " : index === length - 1 ? "." : ", ";
}