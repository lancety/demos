
const month: string[] =
	[
		"Jan", "Feb", "Mar", "Apr",
		"May", "Jun", "Jul", "Aug",
		"Sep", "Oct", "Nov", "Dec"
	];
export function util_getDate(date: Date): string {
	return month[date.getMonth()] + " " + date.getFullYear();
}