import * as React from "react";
import * as ReactRedux from "react-redux";

import TopMenu from "./components/topMenu";
import Skill from "./components/skill";
import Education from "./components/education";
import Referee from "./components/referee";
import OwnerInfo from "./components/ownerInfo";
import Project from "./components/project";

import {AppSate} from "./models/index";
import Timeline from "./components/timeline";

class App extends React.Component<AppSate, any> {
	constructor(props: AppSate, context: any) {
		super(props, context);
		this.state = {};
	}

	render() {
		const {skill, education, timeline, personInfo, project} = this.props;

		return (
			<div>
				<TopMenu/>

				<Timeline activities={timeline}/>

				<OwnerInfo owner={personInfo.owner}/>

				<Education education={education}/>

				<Skill skill={skill}/>

				<Project projectState={project}/>

				<Referee referees={personInfo.referee}/>
			</div>
		)
	}
}

const mapStateToProps: ReactRedux.MapStateToProps<any, any> = (state: AppSate) => (state);

const mapDispathToProps: ReactRedux.MapDispatchToPropsFunction<any, any> = (dispatch, ownProps) => {
	return {
	}
};

export default ReactRedux.connect(mapStateToProps, mapDispathToProps)(App);