import * as React from "react";
import {createStore, Store} from "redux";
import {render} from "react-dom"
import {Provider} from "react-redux";

import reducer from "./reducers/index";
import App from "./app";
import initState from "./data/index";

const store: Store<{}> = createStore(reducer, initState);

render(
	<Provider store={store}>
		<App/>
	</Provider>,
	document.getElementById("root")
);