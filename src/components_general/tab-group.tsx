import * as React from "React";

interface TabGroupProps {
	categoryChosen: any;
	categories: any[];
	itemsChosen: any[];
	tabClick: (category: any)=> void;
}

class TabGroup extends React.Component<TabGroupProps, any> {
	constructor(props: TabGroupProps, context: any) {
		super(props, context);
	}

	render() {
		const {categoryChosen, categories, itemsChosen, tabClick} = this.props;

		return <div className="btn-group">
			{
				itemsChosen ?
					<button
						className= {`btn btn-sm btn-dark ${categoryChosen == "" ? "active" : ""}`}
						value=""
						onClick={ e => {tabClick("")} }>
						Show All&nbsp;
						<span className="badge">
									{categoryChosen == "" ? itemsChosen.length : ""}
								</span>
					</button> : ""
			}

			{
				categories.map((catString, ind) => {
					return <button
						key={ind}
						className={`btn btn-sm btn-dark ${categoryChosen === catString ? "active" : ""}`}
						value={catString}
						onClick={ e => {tabClick(catString)} }>
						<span className="text-capitalize">{catString}</span>&nbsp;
						<span className="badge">
										{itemsChosen && categoryChosen === catString ?
											itemsChosen.length : ""}
									</span>
					</button>
				})
			}
		</div>
	}
}

export default TabGroup;