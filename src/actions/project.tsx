export const PROJECT_FILTER_UPDATE = "PROJECT_FILTER_UPDATE";

export function projectFilterUpdate(filter: string) {
	return {
		type: PROJECT_FILTER_UPDATE,
		filter: filter
	}
}