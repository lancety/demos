export class Education {
	constructor(public dateStart: Date,
				public dateFinish: Date,
				public school: string,
				public major: string) {

	}
}