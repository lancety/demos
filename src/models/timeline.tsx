export class Activity {
	static keyCounter: number = 0;

	public key: number;

	constructor(public dateStart: Date,
				public description: string) {
		this.key = Activity.keyCounter++;
	}
}



