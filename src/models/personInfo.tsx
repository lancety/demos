export class Address {
	constructor(public street: string,
				public suburbe: string,
				public state: string,
				public post: number) {

	}
}

export class PersonInfo {
	static keyCounter: number = 0;
	public key: number;

	constructor(public name: string,
				public img: string,
				public company: string,
				public position: string,
				public email: string,
				public contact: string,
				public linkedIn: string,
				public resume: string,
				public address: Address) {
		this.key = PersonInfo.keyCounter++;
	}
}

