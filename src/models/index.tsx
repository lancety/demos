import {Skill} from "./skill";
import {Activity} from "./timeline";
import {Education} from "./education";
import {PersonInfo} from "./personInfo";
import { ProjectState} from "./project";

export interface AppSate {
	dispatch?: any;

	tech: any;
	tech2d: any;

	personInfo: {
		owner: PersonInfo;
		referee: PersonInfo[]
	};
	education: Education[];
	skill: Skill[];
	timeline: Activity[];

	project: ProjectState;
}