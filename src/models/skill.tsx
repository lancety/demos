export class Skill {
	static keyCounter: number = 0;
	public key: number;

	constructor(
		public category: string,
		public description: string[]
	){
		this.key = Skill.keyCounter++;
	}
}