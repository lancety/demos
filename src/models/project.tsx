export enum ProjectActionType {
	TRY,
	VIDEO,
	IMAGE
}

export class ProjectAction {
	type: ProjectActionType;
	url?: string;

	constructor(type: ProjectActionType, url?: string) {
		this.type = type;
		this.url = url;
	}
}

export type ProjectPriority = "major" | "prototype" | "hobby" | "";
export const projectPriorityList: ProjectPriority[] = ["major", "prototype", "hobby"];

export class Project {
	constructor(public priority: ProjectPriority,
				public title: string,
				public subTitle: string,
				public img: string,
				public tech: string[],
				public objective: string,
				public completion: string[],
				public role: string,
				public dateStart: Date,
				public action: ProjectAction,
				public git?: string
	) {

	}
}

export interface ProjectState {
	project: Project[],
	projectPriority: ProjectPriority
}