import * as React from "react";
import {
	Project as ProjectClass,
	ProjectActionType,
	ProjectPriority,
	ProjectState as ProjectReduxState,
	projectPriorityList
} from "../models/project";
import {util_getDate} from "../util/date";
import TabGroup from "../components_general/tab-group";
import SkillDescription from "./skillDescription";
import {arraySentenceConnector} from "../util/string";

interface ProjectProps {
	projectState: ProjectReduxState;
}

interface ProjectState {
	projectPriority: ProjectPriority;
	projectsVisible: ProjectClass[];
}

class Project extends React.Component<ProjectProps, ProjectState> {
	constructor(public props: ProjectProps, context: any) {
		super(props, context);

		let defaultProjectPriority: ProjectPriority = this.props.projectState.projectPriority;

		this.state = {
			projectPriority: defaultProjectPriority,
			projectsVisible: Project.filterProjects(this.props.projectState.project, defaultProjectPriority)
		}
	}

	static filterProjects = (allProjects: ProjectClass[], filter: ProjectPriority): ProjectClass[] => {
		if (filter) {
			return allProjects.filter((p: ProjectClass) => p.priority === filter);
		} else {
			return allProjects;
		}
	};

	private onClick_projectCategory = (priorityString: ProjectPriority): void => {
		this.setState({
			projectPriority: priorityString,
			projectsVisible: Project.filterProjects(this.props.projectState.project, priorityString)
		})
	};

	generateHeader() {
		const projectPriority = this.state.projectPriority;
		const projectsFiltered = this.state.projectsVisible;
		const tabClick = this.onClick_projectCategory;

		return <h1 className="text-center">
			Experience&nbsp;
			<div className="text-center">
				<TabGroup categories={projectPriorityList} categoryChosen={projectPriority}
						  itemsChosen={projectsFiltered} tabClick={tabClick}/>
			</div>
		</h1>
	};

	static generateTitle(project: ProjectClass) {
		return <div className="col-xs-12 containerOverflow">
			<h3>
				{`${project.title}`}
				<small>
					{`. ${util_getDate(project.dateStart)}`}
					{project.subTitle ? `. ${project.subTitle}` : ""}
				</small>
			</h3>
		</div>
	};

	static generatePreview(project: ProjectClass) {
		return <div className="col-sm-4 img-panel">
			<img src={project.img} title={project.title} alt={project.title}
				 className="img-panel-img"/>
			<div className="img-panel-btn">
				{
					project.action.url ?
						<a className="btn btn-green cursor-pinter"
						   href={project.action.url}
						   target="_blank">
							{ProjectActionType[project.action.type]}
						</a>
						:
						<label className="cursor-disabled">{ProjectActionType[project.action.type]}</label>
				}
			</div>
		</div>
	};

	static generateInfoPanel(project: ProjectClass) {
		return <div className="col-sm-8">
			<dl className="dl-horizontal dl-sub dl-sub-right">
				{
					project.tech ?
						[
							<dt>Tech</dt>,
							<dd>
								{
									project.tech.map((tech, index: number) => {
										return [
											<SkillDescription description={tech}/>,
											arraySentenceConnector(index, project.tech.length)
										]
									})
								}
							</dd>
						] : ""
				}
				<dt>Objective</dt>
				<dd>{project.objective}</dd>
				{
					project.completion ?
						[

							<dt>Completion</dt>,
							<dd>
								<ul>
									{
										project.completion.map((complete: string) => (
											<li>{complete}</li>
										))
									}
								</ul>
							</dd>
						] : ""
				}
				{
					project.role ?
						[
							<dt>My Role</dt>,
							<dd>{project.role}</dd>
						] : ""
				}
			</dl>
		</div>
	};

	openGitRepo = (project: ProjectClass) => {
		window.open(project.git, "_blank");
	};

	render() {
		const projects = this.state.projectsVisible;

		return (
			<div>
				{this.generateHeader()}
				{
					projects.map((project: ProjectClass, index: number) => {
						const oddRow: boolean = index % 2 === 1;

						return <div className={`containerOverflow listPanel ${oddRow ? "odd-row" : ""}`}>
							{
								project.git ?
									<div className="listPanel-badge-git">
										<button className="icon"
												onClick={ e =>{this.openGitRepo(project)}}>
											<span className="icon-word">Git Repository</span>
										</button>
									</div> : ""
							}
							<div className="listPanel-badge">{projects.length - index}</div>
							{Project.generateTitle(project)}
							{Project.generatePreview(project)}
							{Project.generateInfoPanel(project)}
						</div>
					})
				}
			</div>
		)
	}
}

export default Project;
