import * as React from "react";
import * as d3 from "../../lib/d3";
import {PartitionState} from "./sunburstUtil";
import PathTextBase from "./pathTextBase";
import {Partition} from "./d3_util";

interface PathTextProps {
	clickPath: (p: Partition) => void,
	partition: Partition,
	partitionState: PartitionState,
	partitionBase: Partition,
	id: string
}

class PathText extends React.Component<PathTextProps, any> {
	private d3Text: any;
	private d3TextPath: any;

	constructor(props: PathTextProps, context: any) {
		super(props, context);
	}

	private getClassName(rootTech: string) {
		return "tech-theme-" + rootTech;
	}


	componentDidMount() {
		this.d3Text = d3.Selection.select(this.refs["text"] as any);
		this.d3TextPath = d3.Selection.select(this.refs["textPath"] as any);
	}

	componentDidEnter(callback: any) {
		if (this.props.partition.y0 !== 0) {
			let color = d3.Color.rgb(this.d3Text.style("fill"));
			let newColor = color.brighter(this.props.partition.depth / 4).toString();

			this.d3Text
				.on("click", () => {
					this.props.clickPath(this.props.partition)
				})
				.attr("letter-spacing", "1.5")
				.style("text-anchor", "middle")
				.style("fill", newColor);

			this.d3TextPath
				.attr("xlink:href", () => {
					return `#${this.props.id}`;
				})
				.text(() => {
					return this.props.partition.data.name;
				});

			this.componentDidUpdate();
		}
	}

	componentDidLeave() {
		this.d3Text
			.off("click");
	}

	shouldComponentUpdate(newProps: PathTextProps) {
		if (newProps.partitionState.showText) {
			this.d3Text
				.style("visibility", "visible");
			return true;
		} else {
			this.d3Text
				.style("visibility", "hidden");
			return false;
		}
	}

	componentDidUpdate() {
		this.d3Text
			.style("visibility", this.props.partitionState.showText ? "visible" : "hidden")
			.attr("font-size", this.props.partition === this.props.partitionBase ? "220%" : "120%")
			.attr("dy", this.props.partition === this.props.partitionBase ? "35px" : PathTextBase.isBottom(this.props.partition, this.props.partitionBase) ? "-6px" : "15px");

		let that = this;
		this.d3TextPath
			.attr("startOffset", function (d: any, i: any) {
				return that.props.partition === that.props.partitionBase ? "15%" : "50%";
			});
	}

	render() {
		return <text ref="text" className={this.getClassName(this.props.partition.rootTech)}>
				<textPath ref="textPath"/>
			</text>;
	}
}

export default PathText;