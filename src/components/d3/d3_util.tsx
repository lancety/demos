import * as d3 from "../../lib/d3";
import {ScaleLinear} from "d3-scale";

export interface ScaleXY {
    x: ScaleLinear<number, number>;
    y: ScaleLinear<number, number>;
}

export interface PartitionXY {
    x0: number;
    x1: number;
    y0: number;
    y1: number;
}

export interface Partition extends PartitionXY{
    children: Partition[],
    data: any,
    rootTech?: string,
    depth: number,
    height: number,
    parent: Partition,
    value: number
}

export let getArcFn = (scaleXY: ScaleXY, partitionXY: PartitionXY) => {
    return (d3.Shape.arc() as any)
        .startAngle(function (d: any) {
            return Math.max(0, Math.min(2 * Math.PI, scaleXY.x(partitionXY["x0"])));
        })
        .endAngle(function (d: any) {
            return Math.max(0, Math.min(2 * Math.PI, scaleXY.x(partitionXY["x1"])));
        })
        .innerRadius(function (d: any) {
            return Math.max(0, scaleXY.y(partitionXY["y0"]));
        })
        .outerRadius(function (d: any) {
            return Math.max(0, scaleXY.y(partitionXY["y1"]));
        })();
};