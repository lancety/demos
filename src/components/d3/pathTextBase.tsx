import * as React from "react";
import * as d3 from "../../lib/d3";
import {PartitionState} from "./sunburstUtil";
import {Partition, ScaleXY, getArcFn} from "./d3_util";

interface PathTextBaseProps {
	partitionState: PartitionState,
	partitionBase: Partition,
	partition: Partition,
	scaleXY: ScaleXY,
	id: string
}

class PathTextBase extends React.Component<PathTextBaseProps, any> {
	private d3BasePath: any;


	constructor(props: PathTextBaseProps, context: any) {
		super(props, context);
	}

	public static isBottom = (p: Partition, pb: Partition) => {
		let rightBottom = pb.x0 + (pb.x1 - pb.x0) * 0.25;
		let leftBottom = pb.x0 + (pb.x1 - pb.x0) * 0.75;

		return (p.x0 + p.x1) / 2 > rightBottom && (p.x0 + p.x1) / 2 < leftBottom;
	};

	private flipArc(partition: Partition, d: string, partitionBase: Partition) {
		let firstArcSection = /(^.+?)L/;

		if (partition === partitionBase) {
			return /^(M.*)M/.exec(d) ? /^(M.*)M/.exec(d)[1] : d;
		}

		if (!firstArcSection.exec(d) || firstArcSection.exec(d).length < 2) {
			return d;
		}
		let newArc: any = firstArcSection.exec(d)[1];
		newArc = newArc.replace(/,/g, " ");

		if (!/(A)/.exec(newArc)){
			return d;
		}

		let startLoc = /M(.*?)A/,
			middleLoc = /A(.*?)(0 [01] 1)/,
			endLoc = /(0 [01] 1) (.*?)$/;

		let newStart = endLoc.exec(newArc);
		let middleSec = middleLoc.exec(newArc);
		let newEnd = startLoc.exec(newArc);

		if (PathTextBase.isBottom(partition, partitionBase)) {
			let lastNumber = middleSec[2].match(/\d$/)[0];
			let middleSecThreeDigits = middleSec[2].replace(/\d$/, lastNumber == "1" ? "0" : "1");

			return "M" + newStart[2] + "A" + middleSec[1] + middleSecThreeDigits + " " + newEnd[1];
		} else {
			return "M" + newEnd[1] + "A" + middleSec[1] + middleSec[2] + " " + newStart[2];
		}
	}

	componentDidMount() {
		this.d3BasePath = d3.Selection.select(this.refs["basePath"] as any);
		this.componentDidUpdate();
	}

	shouldComponentUpdate(newProps: PathTextBaseProps){
		return newProps.partitionState.showText;
	}

	componentDidUpdate() {
		let d = getArcFn(this.props.scaleXY, this.props.partition);
			d = this.flipArc(this.props.partition, d, this.props.partitionBase);
		this.d3BasePath
			.attr("d", d);
	}

	render(){
		return <path className="pathTextBase"
			  strokeOpacity="0"
			  fillOpacity="0"
			  ref="basePath"
			  id={this.props.id}>
		</path>
	}
}

export default PathTextBase;