import * as React from "react";
import * as d3 from "../../lib/d3";

import ReactTransitionGroup = require('react-addons-transition-group');
import Path from "./path";
import {SunburstProps, default as SunburstUtil, SunburstState, PartitionState} from "./sunburstUtil";
import PathText from "./pathText";
import PathTextBase from "./pathTextBase";
import {Partition} from "./d3_util";

export const ANIMATE_DURATION = 400;

class D3_Sunburst extends React.Component<SunburstProps, any> {
    private sunburstUtil: SunburstUtil;
    private chosenParition: Partition;
    private timeout_windowResize: any;

    constructor(props: SunburstProps, context: any) {
        super(props, context);
        this.state = {};
        this.sunburstUtil = new SunburstUtil(this.state);
    }

    private getContWidth = (): number => {
        let container = d3.Selection.select(this.refs["d3Cont"] as any);
        return parseInt(container.style("width"));
    };

    private updateState = (state: SunburstState) => {
        this.sunburstUtil.state = (Object as any).assign(this.sunburstUtil.state, state);
        this.setState(state);
    };

    componentWillMount() {
        let updatedProps = this.sunburstUtil.updateProps(this.props);
        let updatedDimension = this.sunburstUtil.getDimension(updatedProps, 280);

        this.updateState({
            props: updatedProps,
            partitions: [] as Partition[],
            partitionsState: [] as PartitionState[],
            dimension: updatedDimension,
            scaleXY: this.sunburstUtil.getScaleXY(updatedDimension.radius),
            transitionX: 0,
            transitionY: 0
        });
    }

    componentDidMount() {
        let dimension = this.sunburstUtil.getDimension(this.state.props, this.getContWidth());
        let partitions = this.sunburstUtil.getPartitions(this.state.props.chatData);
        let partitionsState = partitions.map((partition: Partition) => {
            return {
                showText: this.sunburstUtil.showPartition(partitions[0], partition)
            }
        });

        this.updateState({
            partitions: partitions,
            partitionChosen: partitions[0],
            partitionsState: partitionsState,
            dimension: dimension,
            scaleXY: this.sunburstUtil.getScaleXY(dimension.radius),
            transitionX: dimension.width / 2,
            transitionY: dimension.height / 2
        });

        window.addEventListener("resize", () => {
            if (this.timeout_windowResize) {
                clearTimeout(this.timeout_windowResize);
            }

            let contWidth: number = this.getContWidth();
            if (this.state.dimension.width !== contWidth) {
                this.timeout_windowResize = setTimeout(() => {
                    let dimension = this.sunburstUtil.getDimension(this.state.props, contWidth);
                    let scaleXY = this.sunburstUtil.getScaleXY(dimension.radius, this.state.partitionChosen);
                    this.updateState({
                        dimension: dimension,
                        scaleXY: scaleXY,
                        transitionX: dimension.width / 2,
                        transitionY: dimension.height / 2
                    });
                }, ANIMATE_DURATION)
            }
        })
    }

    componentDidUpdate(props: SunburstProps, state: SunburstState) {
        let scaleXY = this.sunburstUtil.getScaleXY(this.state.dimension.radius, this.state.partitionChosen);
        if (this.chosenParition) {
            d3.Selection.select(this.refs["d3Svg"] as any).transition(
                this.sunburstUtil.getTransition(
                    this.chosenParition,
                    scaleXY,
                    this.state.dimension.radius,
                    ANIMATE_DURATION),
            );
        }
    }

    render() {
        let clickPath = (partition: Partition) => {
            let partitionsState = this.sunburstUtil.batchPartitionsState(this.state.partitionsState, {showText: false});
            this.chosenParition = partition;
            this.updateState({
                partitionsState: partitionsState
            });

            this.timeout_windowResize = setTimeout(() => {
                let scaleXY = this.sunburstUtil.getScaleXY(this.state.dimension.radius, partition);
                this.sunburstUtil.updatePartitionsState(
                    this.state.partitions,
                    partition,
                    partitionsState);

                this.updateState({
                    scaleXY: scaleXY,
                    partitionChosen: partition,
                    partitionsState: partitionsState
                });
            }, ANIMATE_DURATION);
        };

        return <div className="d3-chat-cont" ref="d3Cont">
            <svg className="d3-chat" ref="d3Svg"
                 width={this.state.dimension.width}
                 height={this.state.dimension.height}>
                <g transform={`translate(${this.state.transitionX}, ${this.state.transitionY})`}>

                    <ReactTransitionGroup component="g">
                        {this.state.partitions.map((partition: Partition, ind: number) => {
                            let id = `arc_text_${partition.data.name}_${partition.depth}_${partition.height}_${partition.value}`;
                            return <PathTextBase partition={partition}
                                                 scaleXY={this.state.scaleXY}
                                                 partitionState={this.state.partitionsState[ind]}
                                                 partitionBase={this.state.partitionChosen}
                                                 id={id}
                                                 key={ind}/>;
                        })}
                    </ReactTransitionGroup>

                    <ReactTransitionGroup component="g">
                        {this.state.partitions.map((partition: Partition, ind: number) => {
                            return <Path partition={partition}
                                         scaleXY={this.state.scaleXY}
                                         clickPath={clickPath}
                                         key={ind}/>;
                        })}
                    </ReactTransitionGroup>

                    <ReactTransitionGroup component="g">
                        {this.state.partitions.map((partition: Partition, ind: number) => {
                            let id = `arc_text_${partition.data.name}_${partition.depth}_${partition.height}_${partition.value}`;
                            return <PathText partition={partition}
                                             partitionState={this.state.partitionsState[ind]}
                                             partitionBase={this.state.partitionChosen}
                                             id={id}
                                             clickPath={clickPath}
                                             key={ind}/>;
                        })}
                    </ReactTransitionGroup>
                </g>
            </svg>
        </div>;
    }
}

export default D3_Sunburst;