import * as d3 from "../../lib/d3";
import {Partition} from "./d3_util";
import {getArcFn} from "./d3_util";
import {ScaleXY} from "./d3_util";
import {PartitionXY} from "./d3_util";

export interface SunburstProps {
	chatData: any;
	margin?: number;
	maxWidth?: number;
}

export interface SunburstState {
	props?: SunburstProps;
	dimension?: SunburstDimension;
	transitionX?: number;
	transitionY?: number;
	scaleXY?: ScaleXY;
	partitions?: Partition[];
	partitionsState?: PartitionState[];
	partitionChosen?: Partition;
}

export interface SunburstDimension {
	width: number;
	height: number;
	radius: number;
}

export interface PartitionState {
	showText: any;
}

class SunburstUtil {
	constructor(public state: SunburstState) {

	}

	public updateProps(props: SunburstProps): SunburstProps {
		let propsCatch = (Object as any).assign({}, props);
		propsCatch.maxWidth = props.maxWidth || 400;
		propsCatch.margin = props.margin || 15;

		return propsCatch;
	}

	public getDimension = (tmpProps: SunburstProps, contWidth: number): SunburstDimension => {
		let {maxWidth, margin} = tmpProps;

		let width = contWidth < maxWidth ? contWidth : maxWidth;
		let height = width > document.documentElement.clientHeight ?
			document.documentElement.clientHeight : width;

		let radius = (Math.min(width, height) / 2 ) - margin;

		return {width, height, radius} as SunburstDimension;
	};

	public getScaleXY = (radius: number, partitionXY?:PartitionXY): ScaleXY => {
		let scaleXY = {} as ScaleXY;
		if (partitionXY) {
			scaleXY.x = d3.Scale.scaleLinear().domain([partitionXY.x0, partitionXY.x1]).range([0, 2* Math.PI]);
			scaleXY.y = d3.Scale.scaleLinear().domain([partitionXY.y0, 1]).range([partitionXY.y0 ? 20 : 0, radius]);
		} else {
			scaleXY.x = d3.Scale.scaleLinear().range([0, 2 * Math.PI]);
			scaleXY.y = d3.Scale.scaleLinear().range([0, radius]);
		}

		return scaleXY;
	};

	public getTransition = (d: Partition, scaleXY: ScaleXY, radius: number, duration?: number) => {
		let {x, y} = scaleXY;
		duration = duration || 0;

		return (d3.Transition as any).transition()
			.duration(duration)
			.tween("scale", () => {
				let xd = d3.Interpolate.interpolate(x.domain(), [d.x0, d.x1]),
					yd = d3.Interpolate.interpolate(y.domain(), [d.y0, 1]),
					yr = d3.Interpolate.interpolate(y.range(), [d.y0 ? 20 : 0, radius]);
				return (t: any) => {
					x.domain(xd(t));
					y.domain(yd(t)).range(yr(t));
				};
			})
			.selectAll("path.path").attrTween("d", (d: any, index: any, array: any) => {
				return (t: any) => {
					let {x0, x1, y0, y1} = this.state.partitions[index];
					return getArcFn({x,y}, {x0, x1, y0, y1});
				}
			});
	};

	public getPartitions = (chatData: Object): Partition[] => {
		let dataRoot = d3.Hierarchy.hierarchy(chatData);
		dataRoot.sum((d: any) => d["size"]);
		let partition = d3.Hierarchy.partition();
		let partitions = partition(dataRoot).descendants();

		for (let partition of partitions) {
			this.insertRootTech(partition as any);
			this.simplifyDecimal(partition as any, 4);
		}

		return partitions as any;
	};

	public getPartitionChildren = (partitions: Partition): Partition[] => {
		if (!partitions.children) {
			return [];
		}

		let partitionList: Partition[] = partitions.children;

		for (let child of partitions.children) {
			partitionList = partitionList.concat(this.getPartitionChildren(child));
		}

		return partitionList;
	};

	public batchPartitionsState = (partitionsState: PartitionState[],
								   defaultState: PartitionState): PartitionState[] => {
		let tmpPartitionsState: PartitionState[] = [];
		let index: number = 0;

		for (let state of partitionsState) {
			tmpPartitionsState.push((Object as any).assign({}, state));
			for (let stateKey in defaultState) {
				tmpPartitionsState[index][stateKey] = defaultState.showText;
			}

			index++;
		}

		return tmpPartitionsState;
	};

	public showPartition = (partitionRoot: Partition, partition: Partition, minPercent = 1/15): boolean => {
		return partition.value / partitionRoot.value >= minPercent;
	};

	public updatePartitionsState = (partitions: Partition[],
									chosenPartition: Partition,
									partitionsState: PartitionState[]) => {
		partitionsState.map((p: PartitionState) => {
			p.showText = false;
		});

		let partitionsForUpdating: Partition[];
		if (partitions[0] !== chosenPartition) {
			partitionsForUpdating = [chosenPartition].concat(this.getPartitionChildren(chosenPartition));
		} else {
			partitionsForUpdating = partitions;
		}

		for (let pIndex in partitions) {
			let partition = partitions[pIndex];
			let partitionState = partitionsState[pIndex];

			if (partitions === partitionsForUpdating) {
				partitionState.showText = this.showPartition(chosenPartition, partition);
				continue;
			}

			// nor part of chosen partition
			if (partitionsForUpdating.indexOf(partition) < 0) {
				partitionState.showText = false;
				continue;
			}
			// part of chosen partition
			partitionsState[pIndex].showText = this.showPartition(chosenPartition, partition);
		}
	};

	private insertRootTech = (partition: Partition) => {
		if (partition.parent === null) {

		} else {
			if (partition.depth === 1) {
				partition.rootTech = partition.data.name;
			} else {
				let rootTechPartition: Partition = partition;
				let parentTech: string;
				while (rootTechPartition.depth !== 1) {
					rootTechPartition = partition.parent;
					if (rootTechPartition.rootTech) {
						parentTech = rootTechPartition.rootTech;
						break;
					}
				}
				partition.rootTech = parentTech || rootTechPartition.data.name;
			}
		}
	};

	private simplifyDecimal = (partition: Partition, decimalSpace: number) => {
		partition.x0 = parseFloat(partition.x0.toFixed(decimalSpace));
		partition.x1 = parseFloat(partition.x1.toFixed(decimalSpace));
		partition.y0 = parseFloat(partition.y0.toFixed(decimalSpace));
		partition.y1 = parseFloat(partition.y1.toFixed(decimalSpace));

	};
}

export default SunburstUtil;