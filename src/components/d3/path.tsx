import * as React from "react";
import * as d3 from "../../lib/d3";
import {Partition, getArcFn, ScaleXY} from "./d3_util";

interface PathProps {
	clickPath: (p: Partition) => void,
	partition: Partition,
    scaleXY: ScaleXY
}

class Path extends React.Component<PathProps, any> {
	private d3path: any;

	constructor(props: PathProps, context: any) {
		super(props, context);
	}

	componentDidMount() {
		this.d3path = d3.Selection.select(this.refs["path"] as any);
	}

	componentDidEnter(callback: any) {
		let color = d3.Color.rgb(this.d3path.style("stroke"));
		let newColor = color.brighter(this.props.partition.depth / 4).toString();

		this.d3path
            .attr("d", getArcFn(this.props.scaleXY, this.props.partition))
			.on("click", () => {
				this.props.clickPath(this.props.partition)
			})
			.style("stroke", newColor);
	}

	componentDidLeave(callback: any) {
		this.d3path
			.off("click");
	}

	componentDidUpdate() {
		this.d3path
			.attr("d", getArcFn(this.props.scaleXY, this.props.partition));
	}

	render() {
		return <path ref="path"
					 className={`path tech-theme-${this.props.partition.rootTech}`}/>
	}
}

export default Path;