import * as React from "react";
import {PersonInfo} from "../models/personInfo";

interface PersonSourceProps {
	person: PersonInfo
	containerClass?: string;
}

class PersonSource extends React.Component<PersonSourceProps, any> {
	constructor(props: PersonSourceProps, context: any) {
		super(props, context)
	}

	render() {
		const person = this.props.person;
		const containerClass = this.props.containerClass || "";

		return (<div className={containerClass}>
			<ul className="list-group">
				{
					person.resume ?
						<li className="list-group-item">
							<a href={person.resume} target="_blank">
								<h3
									className="glyphicon glyphicon-file"> Resume
								</h3>
							</a>
						</li> : ""
				}
				{
					person.linkedIn ?
						<li className="list-group-item">
							<a href={person.linkedIn} target="_blank">
								<h3
									className="glyphicon glyphicon-user"> LinkedIn
								</h3>
							</a>
						</li> : ""
				}
			</ul>
		</div>);
	}
}

export default PersonSource;