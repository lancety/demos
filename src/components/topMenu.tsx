import * as React from "react";

interface TopMenuProps {
}

class TopMenu extends React.Component<TopMenuProps, any> {
    constructor(props: TopMenuProps, context: any) {
        super(props, context)
    }

    render() {
        return (<div className="top-menu">
            <div className="top-menu-left">
                <h3>
                    Lancety Projects
                </h3>
            </div>
            <div className="top-menu-right">
                    <span className="btn btn-sm btn-light-dark">
                        <a href="https://gitlab.com/lancety/demos"
                           target="_blank">Gitlab</a>
                    </span>
            </div>
        </div>);
    }
}

export default TopMenu;