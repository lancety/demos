import * as React from "react";
import {TechObject, tech2d} from "../data/tech";
import {iElementFilterConfig, util_insertElement} from "../util/string";

interface iSkillDescriptionProps {
	description: string;
}

class SkillDescription extends React.Component<iSkillDescriptionProps, any> {
	constructor(props: iSkillDescriptionProps, context: any){
		super(props, context);
	}

	static getClassByTechCategory(techObject: TechObject) {
		return `tech-theme-${techObject.root} text-capitalize`;
	}

	static initConfig(desc: string): iElementFilterConfig {
		return {
			substringList: tech2d,
			string: desc,
			tagName: "span",
			getClassFn: SkillDescription.getClassByTechCategory
		}
	}

	render() {
		return <span>{util_insertElement(SkillDescription.initConfig(this.props.description))}</span>
	}
}

export default SkillDescription;