import * as React from "react";
import {Education as EducationClass} from "../models/education";
import {util_getDate} from "../util/date";

interface EducationProps {
	education: EducationClass[];
}

class Education extends React.Component<EducationProps, any> {
	constructor(props: EducationProps, context: any) {
		super(props, context)
	}

	render() {
		const educationList = this.props.education;

		return (<div className="education containerOverflow">
			<h1 className="text-center">Education</h1>
			{
				educationList.map(education => {
					return (
						[
							<div className="col-xs-8">
								{education.school} - {education.major}
							</div>,
							<div className="col-xs-4">{util_getDate(education.dateStart)}
								- {util_getDate(education.dateFinish)}</div>
						]
					);
				})
			}
		</div>);
	}
}

export default Education;