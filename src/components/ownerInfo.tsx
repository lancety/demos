import * as React from "react";
import {PersonInfo as PersonInfoClass} from "../models/personInfo";
import PersonInfo from "./personInfo";
import PersonSource from "./personSource";

interface OwnerInfoProps {
	owner: PersonInfoClass;
}

class OwnerInfo extends React.Component<OwnerInfoProps, any> {
	constructor(props: OwnerInfoProps, context: any) {
		super(props, context)
	}

	render() {
		const owner: PersonInfoClass = this.props.owner;

		return (<div className="containerOverflow">
			<h1 className="text-center">My Info</h1>
			<PersonInfo person={owner} containerClass="col-sm-5"/>
			<div className="col-xs-12 col-sm-3">
				{/*{*/}
					{/*owner.img ?*/}
						{/*<img*/}
							{/*src={owner.img}*/}
							{/*alt="owner's photo"*/}
							{/*style={imgStyle}/>*/}
						{/*: ""*/}
				{/*}*/}
			</div>
			<PersonSource
				containerClass="col-xs-12 col-sm-4"
				person={owner}/>
		</div>);
	}
}

export default OwnerInfo;