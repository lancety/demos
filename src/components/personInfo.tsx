import * as React from "react";
import {PersonInfo as PersonInfoClass, Address} from "../models/personInfo";

interface PersonInfoProps {
	person: PersonInfoClass;
	containerClass?: string;
}

class PersonInfo extends React.Component<PersonInfoProps, any> {
	constructor(props: PersonInfoProps, context: any) {
		super(props, context)
	}


	render() {
		const person = this.props.person;
		const containerClass = this.props.containerClass || "";

		function listKeyValue(person: PersonInfoClass) {
			let domList: any = [];
			for (let key in person) {
				if (!person[key]) {
					continue;
				}
				switch (key) {
					case "key":
					case "img":
					case "resume":
					case "linkedIn":
						break;
					case "name":
						domList.push(<li key={domList.length} className="text-capitalize">
							<h3>{person[key]}</h3>
						</li>);
						break;
					case "address":
						let address: Address = person[key];
						domList.push(<span className="text-capitalize">{key}: </span>);
						domList.push(<span>{address.street}, {address.suburbe}, {address.state} {address.post}</span>);
						break;
					default:
						domList.push(<li key={domList.length}>
							<span className="text-capitalize">{key}: </span>
							<span>{person[key]}</span>
						</li>);
				}
			}

			return domList;
		}

		return (
			<div className={containerClass}>
				<ul className="list-unstyled">
					{listKeyValue(person)}
				</ul>
			</div>);
	}
}

export default PersonInfo;