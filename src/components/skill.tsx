import * as React from "react";
import {Skill as SkillClass} from "../models/skill";
import D3_Sunburst from "./d3/sunburst";
import {tech4chat} from "../data/tech";
import TabGroup from "../components_general/tab-group";
import SkillList from "./skillList";
import SkillDescription from "./skillDescription";

interface SkillProps {
	skill: SkillClass[]
}

interface SkillState {
	skillTabChosen: string;
}

class Skill extends React.Component<SkillProps, SkillState> {
	private skillTabs = {summary: "Summary", chart: "Chart", list: "Skill List"};

	constructor(props: SkillProps, context: any) {
		super(props, context);

		this.state = {
			skillTabChosen: this.skillTabs.summary
		}
	}

	private generateSummary = () => {
		const skills = this.props.skill;
		return <dl className="dl-horizontal">
			{
				skills.map((skill: SkillClass) => {
					return [
						<dt>
							<strong>{skill.category}</strong>
						</dt>,
						<dd>
							{
								skill.description.map(desc => {
									return [
										<SkillDescription description={desc} />,
										<br/>
									];
								})
							}
						</dd>]
				})
			}
		</dl>
	};

	private generateChart = () => {
		return <D3_Sunburst
			chatData={tech4chat}
			maxWidth={500}/>
	};

	private generateTechList = () => {
		return <SkillList/>;
	};

	render() {
		let skillTabClick = (skillTabName: string) => {
			this.setState({
				skillTabChosen: skillTabName
			})
		};
		let skillViewSwitcher = (tabName: string) => {
			switch (tabName) {
				case this.skillTabs.summary:
					return this.generateSummary();
				case this.skillTabs.chart:
					return this.generateChart();
				case this.skillTabs.list:
					return this.generateTechList();
			}
		};

		return (
			<div className="containerOverflow">
				<h1 className="text-center">
					Skill&nbsp;
					<div className="text-center">
						<TabGroup
							categories={Object.keys(this.skillTabs).map(key => this.skillTabs[key])}
							categoryChosen={this.state.skillTabChosen}
							itemsChosen={null}
							tabClick={skillTabClick}/>
					</div>
				</h1>
				{skillViewSwitcher(this.state.skillTabChosen)}
			</div>
		)
	}
}

export default Skill;