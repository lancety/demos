import * as React from "react";
import {Activity as ActivityClass} from "../models/timeline"
import {util_getDate} from "../util/date";
import SkillDescription from "./skillDescription";

interface iActivityProps {
	activities: ActivityClass[];
}
interface  iActivityState {
	visibleActivities: number;
	allActivitiesAreVisible: boolean;
}

class Timeline extends React.Component<iActivityProps, iActivityState> {
	constructor(props: iActivityProps, context: iActivityState) {
		super(props, context);
		this.state = {
			visibleActivities: 5,
			allActivitiesAreVisible: false
		};
	}

	private  minimiseActivityList(){
		this.setState({
			visibleActivities: 5,
			allActivitiesAreVisible: false
		})
	}

	private updateActivityVisibility() {
		let previousVisibleActivities = this.state.visibleActivities;
		let currentVisibleActivities = previousVisibleActivities + 5;
		this.setState({
			visibleActivities: currentVisibleActivities,
			allActivitiesAreVisible: currentVisibleActivities >= this.props.activities.length
		})
	}

	render() {
		const {activities} = this.props;

		return (
			<div className="timeline-container">
				<h1 className="text-center">Recent Activities</h1>
				<dl className="dl-horizontal">
					{
						activities.map((eachActivity: ActivityClass, index: number) => {
							if (index < this.state.visibleActivities) {
								let startDate: string = util_getDate(eachActivity.dateStart);

								return [
									<dt><strong>{startDate}</strong></dt>,
									<dd>
										<SkillDescription key={eachActivity.key}
														  description={eachActivity.description}/>
									</dd>
								]
							}
						})
					}

					<dt className="hidden-xs">
						<div className="text-center btn btn-dark form-control"
							 onClick={e=>{this.minimiseActivityList()}}>
							Less
						</div>
					</dt>
					<dd>
						{
							this.state.allActivitiesAreVisible ?
								<div className="text-center">
									- The End -
								</div> :
								<div
									className="text-center btn btn-dark form-control"
									onClick={e=> {this.updateActivityVisibility()}}>
									More Activities...
								</div>
						}
					</dd>
				</dl>


			</div>
		)
	}
}

export default Timeline;