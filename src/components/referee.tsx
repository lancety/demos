import * as React from "react";
import {PersonInfo as PersonInfoClass} from "../models/personInfo";
import PersonInfo from "./personInfo";

interface RefereeProps {
	referees: PersonInfoClass[];
}

class Referee extends React.Component<RefereeProps, any> {
	constructor(props: RefereeProps, context: any) {
		super(props, context)
	}

	render() {
		const referees: PersonInfoClass[] = this.props.referees;

		return (<div className="containerOverflow">
			<h1 className="text-center">Referee</h1>
			{
				referees.map(referee => {
					return <PersonInfo
						containerClass="col-sm-6"
						key={referee.key}
						person={referee}/>
				})
			}
		</div>);
	}
}

export default Referee;