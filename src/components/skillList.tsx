import * as React from "react";
import {tech} from "../data/tech";
import {util_insertElement, arraySentenceConnector} from "../util/string";
import SkillDescription from "./skillDescription";

class SkillList extends React.Component<any, any> {
	constructor(props: any, context: any) {
		super(props, context);
	}

	render() {
		const techsObject = tech;

		return <dl className="dl-horizontal">
			{
				Object.keys(techsObject).map((techProp: string) => {
					let tech = techsObject[techProp];

					return [
						<dt>
							<strong className="text-capitalize">{techProp}</strong>
						</dt>,
						<dd>
							{
								Object.keys(tech).map((skillProp, ind) => {
									let skill = tech[skillProp];
									return [
										<SkillDescription description={skill}/>,
										arraySentenceConnector(ind, Object.keys(tech).length)
									];
								})
							}
						</dd>]
				})
			}
		</dl>
	}
}

export default SkillList;