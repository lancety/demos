# How to run this project

Download zip file of this project or folk and pull to your local git repo.

## Build your own online CV##

1. Install all node modules .
```
npm install
```

2. Update your info in the files of data folder, then build the project.
```
npm run build
```

3. Put the whole project on a simple http server and view it.


## To run the sample project locally
1. Install production dependencies.
```
npm install --production
```

2. Put the whole project on a simple http server and view it.


# Customise your CV data in this project#
!! all data files follow specific data structure, please view the typescript class definition in **model** folder.

## Date File description
* **education** - your school / education experience
* **history** - the timeline about when and what you learned in the past
* **index** - only thing need to be changed is project - **projectPriority**: which is the default view under project section.
* **personInfo** - yours and your referees' detail
* **project** - all projects has been done by you. project's priority will be used for categorising on the project section.
* **skill** - might be confused when you see next file "tech", skill file is a summary of what you are good at.
* **tech** - is a whole list of what skills you know. 

## Features which will be affected by your data input
- data/tech.tsx
   - "tech" object structure can't be changed, must be root object and one level of children objects
   - "techSorted" object specifies how your skill chart will looks like on the page (the d3js sunburst chart), more specifically this object defines the structure of your skil sunburst chart.
   - Don't necessary to change any function definitions.
- less/custom.less
   - depends on the root level property names you defined in "tech" object, set theme color in this file in the format of "tech-theme-{property name}", so all mentioned tech/skills will be shown with defined colors.